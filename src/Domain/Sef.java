package Domain;

import java.util.Objects;

public class Sef {
    String nume;

    @Override
    public String toString() {
        return "Nume sef: " + nume + '\n';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sef sef = (Sef) o;
        return Objects.equals(nume, sef.nume);
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Sef(String nume) {
        this.nume = nume;
    }
}
