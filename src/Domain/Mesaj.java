package Domain;

import java.util.Objects;

public class Mesaj {
    String expeditor;
    String mesaj;
    String receptioner;

    @Override
    public String toString() {
        return expeditor + " # " + mesaj + System.lineSeparator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mesaj mesaj1 = (Mesaj) o;
        return Objects.equals(expeditor, mesaj1.expeditor) &&
                Objects.equals(mesaj, mesaj1.mesaj) &&
                Objects.equals(receptioner, mesaj1.receptioner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(expeditor, mesaj, receptioner);
    }

    public String getExpeditor() {
        return expeditor;
    }

    public void setExpeditor(String expeditor) {
        this.expeditor = expeditor;
    }

    public String getMesaj() {
        return mesaj;
    }

    public void setMesaj(String mesaj) {
        this.mesaj = mesaj;
    }

    public String getReceptioner() {
        return receptioner;
    }

    public void setReceptioner(String receptioner) {
        this.receptioner = receptioner;
    }

    public Mesaj(String expeditor, String mesaj) {
        this.expeditor = expeditor;
        this.mesaj = mesaj;
    }
    }
