package Domain;

import java.util.Objects;

public class Echipa {
    String nume;
    TipEchipa tipEchipa;

    public Echipa(String nume, TipEchipa tipEchipa) {
        this.nume = nume;
        this.tipEchipa = tipEchipa;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public TipEchipa getTipEchipa() {
        return tipEchipa;
    }

    public void setTipEchipa(TipEchipa tipEchipa) {
        this.tipEchipa = tipEchipa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Echipa echipa = (Echipa) o;
        return Objects.equals(nume, echipa.nume) &&
                tipEchipa == echipa.tipEchipa;
    }

    @Override
    public String toString() {
        return nume + " # " + tipEchipa + '\n';
    }
}
