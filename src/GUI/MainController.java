package GUI;

import Service.EchipaProiectController;
import Service.EchipaService;
import Service.SefService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MainController {
    boolean sef = false;
    @FXML
    Button sefButton, echipeButton;
    SefController sefController = new SefController();
    EchipaController echipeController = new EchipaController();
    SefService sefService = new SefService();
    EchipaService echipaService = new EchipaService();



    public void initialize() {


        sefButton.setOnAction(e->{
            if (!sef){
                FXMLLoader loader = new FXMLLoader(getClass().getResource("sef.fxml"));
                Stage primaryStage = new Stage();
                AnchorPane root = null;
                try {
                    loader.setRoot(root);
                    loader.setController(sefController);
                    loader.load();

                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                sefController.setEchipaController(echipeController);
                sefController.setEchipaService(echipaService);
                sefController.setSefService(sefService);
                primaryStage.setTitle("Meniu sef");
                primaryStage.setScene(new Scene(loader.getRoot()));
                primaryStage.show();

                sef = true;
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Eroare");
                alert.setContentText("Exista deja un sef!");
                alert.showAndWait();
            }
        });

        echipeButton.setOnAction(e -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("echipe.fxml"));
            Stage primaryStage = new Stage();
            AnchorPane root = null;
            try {
                loader.setRoot(root);
                loader.setController(echipeController);
                loader.load();

            } catch (IOException e1) {
                e1.printStackTrace();
            }

            echipeController.setSefController(sefController);
            echipeController.setEchipaService(echipaService);
            echipeController.setSefService(sefService);
            primaryStage.setTitle("Meniu sef");
            primaryStage.setScene(new Scene(loader.getRoot()));
            primaryStage.show();
        });

    }
}
