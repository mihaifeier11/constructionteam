package GUI;

import Domain.Echipa;
import Domain.TipEchipa;
import Service.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EchipaController implements Observer<MesajChangeEvent> {
    ObservableList<String> echipe = FXCollections.observableArrayList();

    EchipaService echipaService;
    SefService sefService;

    public List<EchipaProiectController> getEchipeProiectController() {
        return echipeProiectController;
    }

    List<EchipaProiectController> echipeProiectController = new ArrayList<>();

    SefController sefController;

    @FXML
    ComboBox<String> echipaComboBox;

    @FXML
    TextField numeTextField, tipTextField;

    @FXML
    Button adaugaButton, alegeButton;


    public void setEchipaService(EchipaService echipaService) {
        this.echipaService = echipaService;
        echipaService.getEchipe().forEach(e ->
                echipe.add(e.toString()));

        echipaComboBox.setItems(echipe);
    }

    public void setSefService(SefService sefService) {
        this.sefService = sefService;
    }

    public void setSefController(SefController sefController) {
        this.sefController = sefController;
        echipaService = new EchipaService();
        echipaService.addObserver(sefController);
        System.out.println("PNM");
        System.out.println(echipaService);

    }

    public EchipaController() {


    }

    public void initialize() {

        adaugaButton.setOnAction(e -> {
            Echipa echipa = new Echipa(numeTextField.getText(), TipEchipa.valueOf(tipTextField.getText()));
            boolean accepted = sefController.adaugaEchipa(echipa);
            if (accepted) {
                try {
                    echipaService.adaugaEchipa(echipa);
                } catch (IOException e1) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Eroare!");
                    alert.setContentText(e1.getMessage());
                }
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Succes!");
                alert.setContentText("Ati fost acceptat!");
                alert.showAndWait();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("echipaProiect.fxml"));
                Stage primaryStage = new Stage();
                AnchorPane root = null;
                EchipaProiectController echipaProiectController = new EchipaProiectController(echipa, sefController, this);
                try {
                    loader.setRoot(root);
                    loader.setController(echipaProiectController);
                    loader.load();

                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                primaryStage.setTitle("Meniu e");
                primaryStage.setScene(new Scene(loader.getRoot()));
                primaryStage.show();

                echipeProiectController.add(echipaProiectController);
                sefController.addEchipaProiectController(echipaProiectController);
                echipaService.addObserver(echipaProiectController);


            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Eroare!");
                alert.setContentText("Nu ati fost acceptat.");
                alert.showAndWait();
            }
        });

        alegeButton.setOnAction(e -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("echipaProiect.fxml"));
            Stage primaryStage = new Stage();
            AnchorPane root = null;
            String nume = echipaComboBox.getValue().split(" # ")[0];
            TipEchipa tipEchipa = TipEchipa.valueOf(echipaComboBox.getValue().split(" # ")[1].replace("\n", ""));
            Echipa echipa = new Echipa(nume, tipEchipa);
            EchipaProiectController echipaProiectController = new EchipaProiectController(echipa, sefController, this);
            try {
                loader.setRoot(root);
                loader.setController(echipaProiectController);
                loader.load();

            } catch (IOException e1) {
                e1.printStackTrace();
            }

            primaryStage.setTitle("Meniu sef");
            primaryStage.setScene(new Scene(loader.getRoot()));
            primaryStage.show();

            echipeProiectController.add(echipaProiectController);
            sefController.addEchipaProiectController(echipaProiectController);
            echipaService.addObserver(echipaProiectController);
        });

    }

    public void stergeEchipa(Echipa echipa) {
        try {
            echipe.remove(echipa);
            echipaService.stergeEchipa(echipa);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ObservableList<Echipa> getEchipe() throws IOException {
        return echipaService.getEchipe();
    }

    @Override
    public void update(MesajChangeEvent mesajChangeEvent) {

    }
}
