package GUI;

import Domain.Echipa;
import Domain.Mesaj;
import Domain.Sef;
import Domain.TipEchipa;
import Repository.MesajRepository;
import Service.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SefController implements Observer<MesajChangeEvent> {
    ObservableList<Mesaj> mesaje = FXCollections.observableArrayList();
    List<EchipaProiectController> echipeProiectController = new ArrayList<>();
    Echipa toateEchipele = new Echipa("Toate echipele", TipEchipa.Toate);
    MesajRepository mesajRepo = new MesajRepository();
    SefService sefService;
    EchipaService echipaService;

    EchipaController echipaController;

    ObservableList<Echipa> echipe = FXCollections.observableArrayList();

    EchipaProiectController echipaProiectController;

    @FXML
    TableColumn<Echipa, String> tipEchipaTableColumn, numeEchipaTableColumn;

    @FXML
    TableView<Mesaj> mesajeTable;

    @FXML
    TableView<Echipa> echipeTable;

    @FXML
    Button trimiteButton, pleacaButton;

    @FXML
    TextArea mesajTextArea;

    @FXML
    ComboBox<Echipa> echipaComboBox;

    @FXML
    TableColumn<Mesaj, String> expeditorTableColumn, mesajTableColumn;

    public void mesajNou(Mesaj mesaj) {
        mesaje.add(mesaj);
        mesajeTable.setItems(mesaje);

    }


    public void addEchipaProiectController(EchipaProiectController echipaProiectController){
        echipeProiectController.add(echipaProiectController);
    }

    public void setSefService(SefService sefService) {
        this.sefService = sefService;
        sefService.addObserver(this);
    }

    public void setEchipaService(EchipaService echipaService) {
        this.echipaService = echipaService;
        echipeTable.setItems(echipaService.getEchipe());

        echipe.clear();
        echipe.add(toateEchipele);
        echipe.addAll(echipaService.getEchipe());
        echipaComboBox.setItems(echipe);
        echipaComboBox.getSelectionModel().selectFirst();
    }


    public void setEchipaController(EchipaController echipaController) {
        this.echipaController = echipaController;
    }

    public void initialize() throws IOException {
        numeEchipaTableColumn.setCellValueFactory(new PropertyValueFactory<>("nume"));

        tipEchipaTableColumn.setCellValueFactory(new PropertyValueFactory<>("tipEchipa"));
        echipeTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        expeditorTableColumn.setCellValueFactory(new PropertyValueFactory<>("expeditor"));
        mesajTableColumn.setCellValueFactory(new PropertyValueFactory<>("mesaj"));
        echipeTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        trimiteButton.setOnAction(e -> {
            System.out.println("HEELLO");
            Mesaj mesaj = new Mesaj("Sef", mesajTextArea.getText());
            if (echipaComboBox.getValue().equals(toateEchipele)){
                System.out.println("sal");
                echipeProiectController.forEach(ecp -> {
                    ecp.mesajNou(mesaj);
                    this.mesajNou(mesaj);
                });
            } else {
               echipeProiectController.forEach(echipa -> {
                   if (echipa.getEchipa().equals(echipaComboBox.getValue())){
                       echipa.mesajNou(mesaj);
                       this.mesajNou(mesaj);
                   }
               });

            }
        });

        pleacaButton.setOnAction(e -> {
            try {
                mesajRepo.save(mesaje);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });


    }

    public boolean adaugaEchipa(Echipa echipa) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Adaugati echipa?");
        alert.setContentText(echipa.toString());

        Optional<ButtonType> button = alert.showAndWait();

        if (button.get() == ButtonType.OK) {
            return true;
        } else if (button.get() == ButtonType.CANCEL){
            return false;
        }

        //return false;
        return false;
    }

    @Override
    public void update(MesajChangeEvent mesajChangeEvent) {
        System.out.println("Here");
        echipeTable.getItems().clear();


        echipeTable.setItems(echipaService.getEchipe());
        echipaComboBox.getItems().clear();
        echipe.clear();
        echipe.add(toateEchipele);
        echipe.addAll(echipaService.getEchipe());
        echipaComboBox.setItems(echipe);
        System.out.println(echipe);
        echipaComboBox.getSelectionModel().selectFirst();
    }
}
