package Service;

import Domain.Echipa;
import Repository.EchipaRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EchipaService implements Observable<MesajChangeEvent> {
    List<Observer<MesajChangeEvent>> mesajObservers = new ArrayList<>();
    ObservableList<Echipa> echipe = FXCollections.observableArrayList();
    EchipaRepository echipaRepo = new EchipaRepository();

    public EchipaService() {
        try {
            echipe = FXCollections.observableArrayList(echipaRepo.findAll().stream().collect(Collectors.toList()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void adaugaEchipa(Echipa echipa) throws IOException {
        echipe.add(echipa);
        echipaRepo.save(echipa);
        notifyObservers(new MesajChangeEvent(ChangeEvent.ADD, echipa));
    }

    public void stergeEchipa(Echipa echipa) throws IOException {
        echipe.remove(echipa);
        echipaRepo.remove(echipa);
        notifyObservers(new MesajChangeEvent(ChangeEvent.ADD, echipa));
    }

    public ObservableList<Echipa> getEchipe() {
        echipe.clear();
        try {
            echipaRepo.findAll().forEach(e -> {
                echipe.add(e);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return echipe;
    }

    @Override
    public void addObserver(Observer<MesajChangeEvent> e) {
        mesajObservers.add(e);
    }

    @Override
    public void removeObserver(Observer<MesajChangeEvent> e) {
        mesajObservers.remove(e);
    }

    @Override
    public void notifyObservers(MesajChangeEvent t) {
        mesajObservers.forEach(o -> o.update(t));
    }
}
