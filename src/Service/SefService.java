package Service;

import Domain.Mesaj;

import javax.print.attribute.standard.MediaSize;
import java.util.ArrayList;
import java.util.List;

public class SefService implements Observable<MesajChangeEvent>{
    List<Mesaj> mesajePrimite = new ArrayList<>();
    List<Observer<MesajChangeEvent>> mesajObservers = new ArrayList<>();

    public void mesajNou(Mesaj mesaj) {
        mesajePrimite.add(mesaj);
        notifyObservers(new MesajChangeEvent(ChangeEvent.ADD, mesaj));
    }



    @Override
    public void addObserver(Observer<MesajChangeEvent> e) {
        mesajObservers.add(e);
    }

    @Override
    public void removeObserver(Observer<MesajChangeEvent> e) {
        mesajObservers.remove(e);
    }

    @Override
    public void notifyObservers(MesajChangeEvent t) {
        mesajObservers.forEach( e-> e.update(t));
    }
}
