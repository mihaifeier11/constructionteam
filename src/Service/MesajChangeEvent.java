package Service;

import Domain.Mesaj;

public class MesajChangeEvent implements Event {
    private ChangeEvent type;
    private Object data, oldData;

    public MesajChangeEvent(ChangeEvent type, Object data) {
        this.type = type;
        this.data = data;
    }

    public MesajChangeEvent(ChangeEvent type, Object data, Object oldData) {
        this.type = type;
        this.data = data;
        this.oldData = oldData;
    }

    public ChangeEvent getType() {
        return type;
    }

    public Object getData() {
        return data;
    }

    public Object getOldData() {
        return oldData;
    }
}
