package Service;

import Domain.Echipa;
import Domain.Mesaj;
import GUI.EchipaController;
import GUI.SefController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;

public class EchipaProiectController implements Observer<MesajChangeEvent> {
    SefController sefController;
    EchipaController echipaController;

    public void setEchipa(Echipa echipa) {
        this.echipa = echipa;
    }

    public Echipa getEchipa() {
        return echipa;
    }

    Echipa echipa;

    ObservableList<Mesaj> mesaje = FXCollections.observableArrayList();

    @FXML
    TableView echipeTable, mesajeTable;

    @FXML
    TableColumn<Echipa, String> tipEchipaTableColumn, numeEchipaTableColumn;

    @FXML
    TableColumn<Mesaj, String> expeditorTableColumn, mesajTableColumn;

    @FXML
    Button trimiteButton, pleacaButton;

    @FXML
    TextArea mesajTextArea;

    public void mesajNou(Mesaj mesaj) {
        mesaje.add(mesaj);
        mesajeTable.setItems(mesaje);

    }


    public EchipaProiectController(Echipa echipa, SefController sefController, EchipaController echipaController) {
        this.echipa = echipa;
        this.sefController = sefController;
        this.echipaController = echipaController;
    }

    public void initialize() {
        numeEchipaTableColumn.setCellValueFactory(new PropertyValueFactory<>("nume"));
        tipEchipaTableColumn.setCellValueFactory(new PropertyValueFactory<>("tipEchipa"));
        echipeTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        expeditorTableColumn.setCellValueFactory(new PropertyValueFactory<>("expeditor"));
        mesajTableColumn.setCellValueFactory(new PropertyValueFactory<>("mesaj"));
        echipeTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        try {
            echipeTable.setItems(echipaController.getEchipe());
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
        }

        trimiteButton.setOnAction(e -> {
            Mesaj mesaj = new Mesaj(echipa.getNume(), mesajTextArea.getText());
            echipaController.getEchipeProiectController().forEach(epc -> {
                epc.mesajNou(mesaj);
                sefController.mesajNou(mesaj);
            });
        });

        pleacaButton.setOnAction(e -> {
            echipaController.stergeEchipa(echipa);
        });
    }


    @Override
    public void update(MesajChangeEvent mesajChangeEvent) {
        echipeTable.getItems().clear();
        try {
            echipeTable.setItems(echipaController.getEchipe());
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
        }

    }
}
