package Repository;

import Domain.Echipa;
import Domain.Mesaj;
import Domain.TipEchipa;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class MesajRepository {
//    public List<Mesaj> findAll() throws FileNotFoundException {
//        String pathname = "src/Repository/mesaje.txt";
//        File file = new File(pathname);
//        List<Mesaj> allMesaje = new ArrayList<>();
//
//        Scanner sc = new Scanner(file);
//        Mesaj m;
//        String tempString;
//        while (sc.hasNextLine()) {
//            tempString = sc.nextLine();
//            m = new Mesaj(tempString.split(" # ")[0], tempString.split(" # ")[1]);
//            allMesaje.add(m);
//        }
//
//        sc.close();
//        return allMesaje;
//    }

    public void save(List<Mesaj> allMesaje) throws IOException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH-mm-ss");
        LocalDateTime now = LocalDateTime.now();

        String pathname = "src/Repository/" + dtf.format(now) + ".txt";
        File file = new File(pathname);
        FileWriter fw = new FileWriter(file);

        allMesaje.forEach(m -> {
            try {
                fw.write(m.toString());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        fw.close();
    }
}
