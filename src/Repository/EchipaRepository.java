package Repository;

import Domain.Echipa;
import Domain.TipEchipa;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EchipaRepository {
    public List<Echipa> findAll() throws IOException {
        String pathname = "src/Repository/echipe.txt";
        File file = new File(pathname);
        List<Echipa> allEchipe = new ArrayList<>();

        Scanner sc = new Scanner(file);
        Echipa e;
        String tempString;
        while (sc.hasNextLine()) {

            tempString = sc.nextLine();
            e = new Echipa(tempString.split(" # ")[0], TipEchipa.valueOf((tempString.split(" # ")[1])));
            allEchipe.add(e);
        }

        sc.close();
        return allEchipe;

    }

    public void save(Echipa echipa) throws IOException {
        String pathname = "src/Repository/echipe.txt";
        File file = new File(pathname);

        FileWriter fw = new FileWriter(file, true);

        fw.append(echipa.toString());


        fw.close();
    }

    public void remove(Echipa echipa) throws IOException {
        ArrayList<Echipa> toateEchipele = (ArrayList<Echipa>) findAll();
        String pathname = "src/Repository/echipe.txt";
        File file = new File(pathname);

        FileWriter fw = new FileWriter(file);

        toateEchipele.forEach(e -> {
            if (!e.equals(echipa)) {
                try {
                    fw.write(e.toString());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

        });



        fw.close();

    }
}
